#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );
extern inline block_size to_block_size(size_t bytes);
extern inline block_capacity to_block_capacity(size_t bytes);
extern inline struct block_header* block_get_header(void* contents);

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
struct region alloc_region  ( void const * addr, size_t query ) {
  size_t const region_size = region_actual_size(query);
  void* region_start = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);

  if (errno == EEXIST) {
      region_start = map_pages(NULL, query, NO_FLAGS);
  }
  if (region_start == MAP_FAILED) return REGION_INVALID;

  block_init(region_start, to_block_size(region_size), NULL);

  return (struct region) {
      .addr = region_start,
      .size = query,
      .extends = addr != HEAP_START
  };
}

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */


static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_splittable(block, query)) return false;

  const block_size old_block_size = size_from_capacity(block->capacity);
  block->capacity = to_block_capacity(query);

  void* rest_block_addr = block_after(block);
  const block_size rest_size = to_block_size(old_block_size.bytes - size_from_capacity(block->capacity).bytes);
  block_init(rest_block_addr, rest_size, block->next);

  block->next = rest_block_addr;

  return true;
}


/*  --- Слияние соседних свободных блоков --- */

void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static block_capacity get_merged_block_capacity(struct block_header* block, struct block_header const* merged_block) {
  return to_block_capacity(block->capacity.bytes + size_from_capacity(merged_block->capacity).bytes);
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block->next == NULL || !mergeable(block, block->next)) return false;

  struct block_header const* next_block = block->next;
  block->capacity = get_merged_block_capacity(block, block->next);
  block->next = next_block->next;

  return true;
}

static bool merge_all_free_next_blocks(struct block_header* block) {
  bool result = try_merge_with_next(block);
  while (try_merge_with_next(block));

  return result;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* block, size_t sz )    {
  struct block_header* it = block;
  while (1) {
    if (it->is_free) {
      merge_all_free_next_blocks(it);
      if (block_is_big_enough(sz, it)) {
        split_if_too_big(it, sz);
        return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = it };
      }
    }

    if (it->next == NULL) break;
    it = it->next;
  }

  return (struct block_search_result) { .type = BSR_REACHED_END_NOT_FOUND, .block = it };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  const struct block_search_result search_result = find_good_or_last(block, query);
  if (search_result.type == BSR_FOUND_GOOD_BLOCK) search_result.block->is_free = false;
  return search_result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  const struct region new_region = alloc_region(block_after(last), query);
  if (region_is_invalid(&new_region)) return NULL;

  last->next = new_region.addr;

  if (try_merge_with_next(last)) {
    return last;
  }
  return last->next;
}

static struct block_search_result try_memalloc_with_heap_growing(size_t query, struct block_header* restrict block) {
  struct block_header* extended_heap_start = grow_heap(block, size_from_capacity(to_block_capacity(query)).bytes);
  return try_memalloc_existing(query, extended_heap_start);
}



/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  struct block_search_result search_result = try_memalloc_existing(query, heap_start);

  struct block_header* result = NULL;
  switch (search_result.type) {
    case BSR_FOUND_GOOD_BLOCK:

      result = search_result.block;
      break;
    case BSR_REACHED_END_NOT_FOUND:
      search_result = try_memalloc_with_heap_growing(query, search_result.block);
      result = search_result.type == BSR_FOUND_GOOD_BLOCK ? search_result.block : NULL;
      break;
    case BSR_CORRUPTED:
      result = NULL;
      break;
    default:
      break;
  }
  return result;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;

  merge_all_free_next_blocks(header);
}

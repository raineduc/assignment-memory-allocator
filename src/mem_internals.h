#ifndef _MEM_INTERNALS_
#define _MEM_INTERNALS_

#include <stddef.h>
#include <stdbool.h>
#include <inttypes.h>

#define REGION_MIN_SIZE (2 * 4096)

struct region { void* addr; size_t size; bool extends; };
static const struct region REGION_INVALID = {0};

inline bool region_is_invalid( const struct region* r ) { return r->addr == NULL; }

typedef struct { size_t bytes; } block_capacity;
typedef struct { size_t bytes; } block_size;

struct block_header {
  struct block_header*    next;
  block_capacity capacity;
  bool           is_free;
  uint8_t        contents[];
};

struct region alloc_region  ( void const * addr, size_t query );
void* block_after( struct block_header const* block );

inline block_size size_from_capacity( block_capacity cap ) { return (block_size) {cap.bytes + offsetof( struct block_header, contents ) }; }
inline block_capacity capacity_from_size( block_size sz ) { return (block_capacity) {sz.bytes - offsetof( struct block_header, contents ) }; }

inline block_size to_block_size(size_t bytes) { return (block_size) { .bytes = bytes }; }
inline block_capacity to_block_capacity(size_t bytes) { return (block_capacity) { .bytes = bytes }; }
inline struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

#endif

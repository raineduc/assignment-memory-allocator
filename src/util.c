#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  abort();
}

void debug(const char* fmt, ... ) {
#ifdef DEBUG

  va_list args;
  va_start (args, fmt);
  vfprintf(stderr, fmt, args);
  va_end (args);

#else
  (void) fmt;
#endif
}


extern inline size_t size_max( size_t x, size_t y );

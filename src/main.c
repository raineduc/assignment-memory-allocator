#include <stdio.h>
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

size_t heap_size = 16000;

//test 1
void alloc_test(void* heap) {
  printf("-----  Start test 1 -----\n");

  const size_t capacity = 1000;

  void* mem = _malloc(capacity);
  if (mem == NULL) {
    debug_heap(stderr, heap);
    err("Test 1: Allocation test failed\n");
  }


  struct block_header* block = block_get_header(mem);
  if (block->is_free) err("Test 1: The block wasn't taken\n");
  if (block->capacity.bytes != capacity) {
    err("Test 1: block capacity isn't equal to old capacity\nExpected: %lu, got: %lu\n", capacity, block->capacity.bytes);
  }

  printf("Test 1: memory allocated successful\n\n");
  debug_heap(stdout, block);
  _free(mem);

  printf("----- End test 1 -----\n\n\n");
}

//test 2
void free_test(void* heap) {
  printf("-----  Start test 2 -----\n");

  const size_t capacity = 1000;

  void* mem1 = _malloc(capacity);
  void* mem2 = _malloc(capacity);
  void* mem3 = _malloc(capacity);
  if (mem2 == NULL) {
    debug_heap(stderr, heap);
    err("Test 2: Allocation failed\n");
  }

  struct block_header* block = block_get_header(mem2);
  _free(mem2);

  if (block_get_header(mem2) != block) err("Test 2: memory was corrupted\n");
  if (!block->is_free) err("Test 2: The block wasn't freed\n");
  if (block->capacity.bytes != capacity) {
    err("Test 2: block capacity isn't equal to old capacity\nExpected: %lu, got: %lu\n", capacity, block->capacity.bytes);
  }
  printf("Test 2: memory freed successful\n\n");
  debug_heap(stdout, heap);
  _free(mem3);
  _free(mem1);

  printf("----- End test 2 -----\n\n\n");
}

//test 3
void free_several_blocks_test(void* heap) {
  printf("-----  Start test 3 -----\n");

  const size_t capacity = 1000;

  void* mem1 = _malloc(capacity);
  void* mem2 = _malloc(capacity);
  void* mem3 = _malloc(capacity);
  if (mem2 == NULL) {
    debug_heap(stderr, heap);
    err("Test 3: Allocation failed\n");
  }

  struct block_header* block2 = block_get_header(mem2);
  struct block_header* block3 = block_get_header(mem3);
  struct block_header* rest_block = block3->next;
  const block_capacity block3_capacity = block3->capacity;
  const block_capacity rest_block_capacity = rest_block->capacity;

  _free(mem3);
  _free(mem2);

  if (block_get_header(mem2) != block2) err("Test 3: memory was corrupted\n");
  if (!block2->is_free) err("Test 3: The block2 wasn't freed\n");
  const size_t expected_capacity = capacity + size_from_capacity(block3_capacity).bytes + size_from_capacity(rest_block_capacity).bytes;

  if (block2->capacity.bytes != expected_capacity) {
    err("Test 3: block capacity isn't equal to old capacity\nExpected: %lu, got: %lu\n", expected_capacity, block2->capacity.bytes);
  }
  printf("Test 3: memory freed successful\n\n");
  debug_heap(stdout, heap);
  _free(mem1);

  printf("----- End test 3 -----\n\n\n");
}

//test 4
void region_not_enough_memory_test(void* heap) {
  printf("-----  Start test 4 -----\n");

  void* mem1 = _malloc(13000);
  void* mem2 = _malloc(13000);

  if (mem1 == NULL || mem2 == NULL) {
    debug_heap(stderr, heap);
    err("Test 3: Allocation failed\n");
  }

  struct block_header* block1 = block_get_header(mem1);
  struct block_header* block2 = block_get_header(mem2);

  if (block2->is_free) err("Test 4: The block wasn't taken\n");
  if (block2->capacity.bytes != 13000) {
    err("Test 4: block capacity isn't equal to old capacity\nExpected: %lu, got: %lu\n", 13000, block2->capacity.bytes);
  }
  if (block1->next != block2) err("Regions are not continuous");

  printf("Test 4: heap grew successful\n\n");
  debug_heap(stdout, heap);

  _free(mem2);
  _free(mem1);

  heap_size = size_from_capacity(block1->capacity).bytes;

  debug_heap(stdout, heap);

  printf("----- End test 4 -----\n\n\n");
}

//test 5
void splitted_regions_test(void* heap) {
  printf("-----  Start test 5 -----\n");

  void const* next_region_addr = block_after(heap);

  const struct region next_region = alloc_region(next_region_addr, 1000);

  void* mem1 = _malloc(heap_size / 2 + 100);
  void* mem2 = _malloc(heap_size / 2 + 100);

  if (mem1 == NULL || mem2 == NULL || next_region.size == 0) {
    debug_heap(stderr, heap);
    err("Test 5: Allocation failed\n");
  }

  struct block_header* block1 = block_get_header(mem1);
  struct block_header* block2 = block_get_header(mem2);

  if (block2->is_free) err("Test 5: The block wasn't taken\n");
  if (block2->capacity.bytes != heap_size / 2 + 100) {
    err("Test 2: block capacity isn't equal to old capacity\nExpected: %lu, got: %lu\n", heap_size / 2 + 100, block2->capacity.bytes);
  }
  if (block1->next == block2) err("Regions shouldn't be continuous\n");
  if (!block1->next->is_free) err("Test 5: memory corrupted\n");
  if (block1->next->next != block2) err("Test 5: memory corrupted\n");

  printf("Test 5: heap grew successful\n\n");
  debug_heap(stdout, heap);

  _free(mem1);
  _free(mem2);

  printf("----- End test 5 -----\n\n\n");
}

int main() {
  printf("Start tests\n\n");
  void* heap = heap_init(heap_size);
  debug_heap(stdout, heap);
  printf("\n");

  alloc_test(heap);
  free_test(heap);
  free_several_blocks_test(heap);
  region_not_enough_memory_test(heap);
  splitted_regions_test(heap);

  return 0;
}

